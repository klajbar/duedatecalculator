require "test/unit"
require_relative "duetodate"

class TestDueToDate < Test::Unit::TestCase
    def test_on_the_date
        assert_equal Time.utc(2017,10,25,10), DueToDate.CalculateDueDate(Time.utc(2017,10,25,9),1)
    end

    def test_if_string_given_to_date
        assert_equal Time.utc(2017,10,25,10), DueToDate.CalculateDueDate("2017-10-25 09:00 UTC",1)
    end

    def test_submit_date_before_work_hours
        assert_equal "Submit date is not in work hours", DueToDate.CalculateDueDate(Time.utc(2017,10,25,3),12)
    end

    def test_submit_date_after_work_hours
        assert_equal "Submit date is not in work hours", DueToDate.CalculateDueDate(Time.utc(2017,10,25,19),3)
    end

    def test_submit_date_in_weekend
        assert_equal "Submit date is not in work hours", DueToDate.CalculateDueDate(Time.utc(2017,10,28,11),3)
    end
    
    def test_in_last_hour_of_day
        assert_equal Time.utc(2017,10,26,10), DueToDate.CalculateDueDate(Time.utc(2017,10,25,17),1)
    end
    
    def test_if_overlaps
        assert_equal Time.utc(2017,10,26,11), DueToDate.CalculateDueDate(Time.utc(2017,10,25,16),3)
    end

    def test_if_days_overlap
        assert_equal Time.utc(2017,10,27,10), DueToDate.CalculateDueDate(Time.utc(2017,10,25,9),17)
    end
    
    def test_if_ends_after_weekend
        assert_equal Time.utc(2017,10,31,10), DueToDate.CalculateDueDate(Time.utc(2017,10,25,15),27)
    end
    
    def test_if_new_month
        assert_equal Time.utc(2017,11,6,10), DueToDate.CalculateDueDate(Time.utc(2017,10,25,11),63)
    end

    def test_isweekend_function_for_weekend
        assert_equal true, DueToDate.inWeekend?(Time.utc(2017,10,28))
    end

    def test_isweekend_function_for_weekday
        assert_equal false, DueToDate.inWeekend?(Time.utc(2017,10,26))
    end
    
    def test_if_in_work_hour
        assert_equal true, DueToDate.inWorkHours?(Time.utc(2017,10,26,11))
    end

    def test_if_before_work_hour
        assert_equal false, DueToDate.inWorkHours?(Time.utc(2017,10,26,5))
    end
    
    def test_if_after_work_hour
        assert_equal false, DueToDate.inWorkHours?(Time.utc(2017,10,26,20))
    end

    def test_invalid_date_given
        assert_raises(ArgumentError) { DueToDate.CalculateDueDate("today",10) }
    end
    
    def test_invalid_hours_given
        assert_raises(ArgumentError) { DueToDate.CalculateDueDate("2017-10-26","ten") }
    end

    def test_if_parameter_is_missing
        assert_raises(ArgumentError) { DueToDate.CalculateDueDate("2017-10-26") }
    end
end