require 'time'

class DueToDate
    
    HOUR = 60*60
    DAY  = 24*HOUR
    DAILY_WORKING_HOURS = 8
    END_OF_DAY = 17

    def self.CalculateDueDate(givenDate, givenHours)

        dueDate = Time.parse givenDate.to_s
        hoursToAdd = Integer givenHours

        return "Submit date is not in work hours" unless inWorkHours? dueDate

        daysToAdd = hoursToAdd / DAILY_WORKING_HOURS
        dueDate += (hoursToAdd % DAILY_WORKING_HOURS) * HOUR

        if dueDate.hour > END_OF_DAY
            daysToAdd += 1 
            dueDate -= DAILY_WORKING_HOURS * HOUR
        end

        while daysToAdd > 0 do
            dueDate += DAY
            dueDate += DAY while inWeekend? dueDate
            daysToAdd -= 1
        end

        return dueDate
    end

    private

    def self.inWorkHours?(date)
        return !(inWeekend?(date)) && (END_OF_DAY - DAILY_WORKING_HOURS .. END_OF_DAY).cover?(date.hour)
    end

    def self.inWeekend?(date)
        return [0,6].include?(date.wday)
    end

end